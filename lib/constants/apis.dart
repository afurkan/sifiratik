import 'dart:convert';
import 'package:dio/dio.dart';

class ApiService {
  ApiService({this.isMacellan = false});

  Dio dio = Dio();

  final bool isMacellan;
  //TODO bunları Furkan'dan al
  String user = '';
  String basicAuth = 'Basic ${base64Encode(utf8.encode(''))}';
  final baseUrl = '';
  final baseMacellanUrl = '';
  final String _bearerAuth = '';

  Dio init() {
    dio.options.headers.addAll({
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': isMacellan ? _bearerAuth : basicAuth
    });
    dio.options.baseUrl = isMacellan ? baseMacellanUrl : baseUrl;
    return dio;
  }
}
