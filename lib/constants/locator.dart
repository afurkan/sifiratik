
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:sifiratik/constants/apis.dart';
import 'package:sifiratik/constants/secure_storage.dart';

final getIt = GetIt.instance;

Future<void> locator() async {
  getIt
    ..registerLazySingleton<SecureStorage>(SecureStorage.new)
    ..registerLazySingleton<Dio>(() => ApiService().init())
    ..registerLazySingleton<Dio>(
      () => ApiService(isMacellan: true).init(),
      instanceName: 'macellanDio',
    );

}
