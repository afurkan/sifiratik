import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sifiratik/constants/contants.dart';

class RoundedAppBar extends StatelessWidget implements PreferredSizeWidget {
  // ignore: prefer_const_constructors_in_immutables
  RoundedAppBar({super.key, this.onTap, this.position});
  Function()? onTap;

  Widget? position;
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.width;

    return Stack(
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox.fromSize(
              size: preferredSize,
              child: LayoutBuilder(
                builder: (context, constraint) {
                  final width = constraint.maxWidth * 8;
                  return ClipRect(
                    child: OverflowBox(
                      maxHeight: double.infinity,
                      maxWidth: double.infinity,
                      child: SizedBox(
                        width: width,
                        height: width - 10,
                        child: Padding(
                          padding: EdgeInsets.only(
                            bottom: width / 2 - preferredSize.height / 2,
                          ),
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [Constants.greens, Constants.greens],
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  offset: const Offset(0, 2),
                                ),
                              ],
                              shape: BoxShape.circle,
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
        Container(
          child: position,
        ),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(Platform.isIOS ? 90 : 100);
}
