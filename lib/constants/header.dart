
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:sifiratik/constants/contants.dart';

class Header extends StatelessWidget {
  // ignore: prefer_const_constructors_in_immutables
  Header({super.key, required this.title, this.onRightIconPress});


  final String title;
  final VoidCallback? onRightIconPress;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Constants.greens,
            width: 2,
          ),
        ),
      ),
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.arrow_back_rounded,
                color: Constants.greens2,
                size: 28,
              ),
            ),
          ),
          Expanded(
            flex: 8,
            child: Center(
              child: AutoSizeText(
                title,
                maxLines: 1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  letterSpacing: 2.1,
                  fontSize: 38,
                  fontWeight: FontWeight.w500,
                  color: Constants.greens2,
                ),
              ),
            ),
          ),
          Expanded(
            child: onRightIconPress == null
                ? const SizedBox()
                : Center(
                    child: IconButton(
                      color: Constants.greens2,
                      onPressed: onRightIconPress,
                      icon: const Icon(Icons.calendar_month),
                    ),
                  ),
          )
        ],
      ),
    );
  }
}
