import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:sifiratik/constants/locator.dart';
import 'package:sifiratik/constants/secure_storage.dart';
import 'package:sifiratik/features/app/app.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await locator();
  await dotenv.load(fileName: ".env");
  final _secureStorage = getIt<SecureStorage>();
  final String? _isUserLoggedIn = await _secureStorage.findKey('userId');
  
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((value) => runApp( MyApp(isUserLoggedIn: _isUserLoggedIn,)));
}


 