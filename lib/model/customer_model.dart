import 'package:sifiratik/model/base_response.dart';
import 'package:json_annotation/json_annotation.dart';
part 'customer_model.g.dart';

@JsonSerializable()
class CustomerModel extends BaseResponse{
  
  CustomerModel({required this.data,required super.status, required super.message});
  factory CustomerModel.fromJson(Map<String, dynamic> json) => _$CustomerModelFromJson(json);
  BaseCustomer data;
  Map<String, dynamic> toJson() => _$CustomerModelToJson(this);

  
}
@JsonSerializable()
class BaseCustomer {
  BaseCustomer(
      {required this.id,
      required this.walletId,
      required this.referenceCode,
      required this.qrCode,
      required this.status,
      required this.statusTranslate,
      required this.amount,
      required this.amountShow,
      required this.createdAt,
      required this.completedAt,
      // ignore: non_constant_identifier_names
      required this.expires_at,
      required this.campaign,
      required this.user});
      factory BaseCustomer.fromJson(Map<String, dynamic> json) =>
      _$BaseCustomerFromJson(json);

  final int id;
  @JsonKey(name: "wallet_id")
  final int walletId;
  @JsonKey(name: "reference_code")
  final int referenceCode;
  @JsonKey(name: "qr_code")
  final String qrCode;
  final String status;
  @JsonKey(name: "status_translate")
  final String statusTranslate;
  final String amount;
  @JsonKey(name: "amount_show")
  final String amountShow;
  @JsonKey(name: "created_at")
  final DateTime createdAt;
  @JsonKey(name: "completed_at")
  final DateTime completedAt;
  // ignore: non_constant_identifier_names
  final DateTime expires_at;
  final List<dynamic> campaign;
  final User user;

   Map<String, dynamic> toJson() => _$BaseCustomerToJson(this);
}

@JsonSerializable()
class User {
  User(
     { 
      required this.uuid, 
      required this.firstName,
      required this.lastName,
      required this.fullName, 
      required this.email,
      required this.phone
      }
      );
      factory User.fromJson(Map<String, dynamic> json) =>
      _$UserFromJson(json);
  final String uuid;
  @JsonKey(name: "first_name")
  final String firstName;
  @JsonKey(name: "last_name")
  final String lastName;
  @JsonKey(name: "full_name")
  final String fullName;
  final String email;
  final String phone;
   Map<String, dynamic> toJson() => _$UserToJson(this);
}
