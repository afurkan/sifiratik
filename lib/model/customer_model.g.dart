// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerModel _$CustomerModelFromJson(Map<String, dynamic> json) =>
    CustomerModel(
      data: BaseCustomer.fromJson(json['data'] as Map<String, dynamic>),
      status: json['status'] as String,
      message: json['message'] as String?,
    );

Map<String, dynamic> _$CustomerModelToJson(CustomerModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

BaseCustomer _$BaseCustomerFromJson(Map<String, dynamic> json) => BaseCustomer(
      id: json['id'] as int,
      walletId: json['wallet_id'] as int,
      referenceCode: json['reference_code'] as int,
      qrCode: json['qr_code'] as String,
      status: json['status'] as String,
      statusTranslate: json['status_translate'] as String,
      amount: json['amount'] as String,
      amountShow: json['amount_show'] as String,
      createdAt: DateTime.parse(json['created_at'] as String),
      completedAt: DateTime.parse(json['completed_at'] as String),
      expires_at: DateTime.parse(json['expires_at'] as String),
      campaign: json['campaign'] as List<dynamic>,
      user: User.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BaseCustomerToJson(BaseCustomer instance) =>
    <String, dynamic>{
      'id': instance.id,
      'wallet_id': instance.walletId,
      'reference_code': instance.referenceCode,
      'qr_code': instance.qrCode,
      'status': instance.status,
      'status_translate': instance.statusTranslate,
      'amount': instance.amount,
      'amount_show': instance.amountShow,
      'created_at': instance.createdAt.toIso8601String(),
      'completed_at': instance.completedAt.toIso8601String(),
      'expires_at': instance.expires_at.toIso8601String(),
      'campaign': instance.campaign,
      'user': instance.user,
    };

User _$UserFromJson(Map<String, dynamic> json) => User(
      uuid: json['uuid'] as String,
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      fullName: json['full_name'] as String,
      email: json['email'] as String,
      phone: json['phone'] as String,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'uuid': instance.uuid,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'full_name': instance.fullName,
      'email': instance.email,
      'phone': instance.phone,
    };
