import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:sifiratik/constants/contants.dart';
import 'package:sifiratik/features/confirm/view/payment_result.dart';
import 'package:sifiratik/features/home/view/home_screen.dart';
import 'package:sifiratik/model/selected_product_model.dart';
import 'package:sifiratik/service/confirmation_service.dart';

part 'confirm_state.dart';

class ConfirmCubit extends Cubit<ConfirmState> {
  ConfirmCubit() : super(const ConfirmState());

  final ConfirmationService _confirmationService = ConfirmationService();

  Future<void> onConfirm({
    required BuildContext context,
    required double amount,
    required List<SelectedProductModel> list,
  }) async {
    emit(const ConfirmState(confirmStateStatus: ConfirmStateStatus.loading));
    try {
      final customer = await _confirmationService.confirmUser(
        amount: amount.toString(),
        list: list,
      );

      if(customer['status'] == "success")
      {
      emit(
            const ConfirmState(confirmStateStatus: ConfirmStateStatus.success));
        // ignore: use_build_context_synchronously
        await Navigator.of(context).pushAndRemoveUntil(
          // ignore: inference_failure_on_instance_creation
          MaterialPageRoute(
            builder: (context) => PaymentResult(
              totalPayment: amount,
            ),
          ),
          (Route<dynamic> route) => false,
        );
      }
        
      else
      {
          emit(const ConfirmState(confirmStateStatus: ConfirmStateStatus.error));
      final snackBar = SnackBar(
        content: Text(
          'Hatalı istek',
          style: TextStyle(color: Constants.amaranth),
        ),
        backgroundColor: Colors.grey.shade200,
        margin: const EdgeInsets.symmetric(horizontal: 10),
        behavior: SnackBarBehavior.floating,
        elevation: 0,
        action: SnackBarAction(
          label: "Anasayfaya dön",
          textColor: Colors.red,
          onPressed: () {
            Navigator.of(context).pushAndRemoveUntil(
          // ignore: inference_failure_on_instance_creation
          MaterialPageRoute(
            builder: (context) =>  HomeScreen()
          ),
          (Route<dynamic> route) => false,
        );
          },
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    
      // ignore: use_build_context_synchronously

    } catch (e) {
      emit(const ConfirmState(confirmStateStatus: ConfirmStateStatus.error));
      final snackBar = SnackBar(
        content: Text(
          'Hatalı istek',
          style: TextStyle(color: Constants.amaranth),
        ),
        backgroundColor: Colors.grey.shade200,
        margin: const EdgeInsets.symmetric(horizontal: 10),
        behavior: SnackBarBehavior.floating,
        elevation: 0,
        action: SnackBarAction(
          label: "Anasayfaya dön",
          textColor: Colors.red,
          onPressed: () {
            Navigator.of(context).pushAndRemoveUntil(
          // ignore: inference_failure_on_instance_creation
          MaterialPageRoute(
            builder: (context) =>  HomeScreen()
          ),
          (Route<dynamic> route) => false,
        );
          },
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
