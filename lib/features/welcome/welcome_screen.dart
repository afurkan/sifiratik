import 'package:sifiratik/constants/contants.dart';
import 'package:sifiratik/constants/rounded_app.dart';
import 'package:sifiratik/features/login/view/login_screen.dart';
import 'package:sifiratik/widgets/button_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';


class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: RoundedAppBar(),
      body: OfflineBuilder(
            connectivityBuilder: (
              BuildContext context,
              ConnectivityResult connectivity,
              Widget child,
            ) {
              final connected = connectivity != ConnectivityResult.none;
              return Stack(
                fit: StackFit.expand,
                children: [
                  child,
                  Positioned(
                    height: 32.0,
                    left: 0.0,
                    right: 0.0,
                    top: 0.0,
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 350),
                      color: connected
                          ? const Color.fromRGBO(255, 0, 0, 0.0)
                          : const Color(0xFFEE4400),
                      child: AnimatedSwitcher(
                        duration: const Duration(milliseconds: 350),
                        child: connected
                            ? const SizedBox()
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const <Widget>[
                                  Text('Lütfen internetinizi açınız'),
                                  SizedBox(width: 8.0),
                                  SizedBox(
                                    width: 12.0,
                                    height: 12.0,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2.0,
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                      ),
                    ),
                  ),
                ],
              );
            },
            child: SizedBox(
        height: h * 2,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
            height: h * 0.1,
              ),
              SizedBox(
              width: w * 0.6,
              height: h * 0.4,
              child: Image.asset(
                "assets/atik_hesapla.png",
                fit: BoxFit.contain,
              )),
              
              SizedBox(
              child: Text("Hoşgeldiniz",style: TextStyle(fontSize: w * 0.113,fontWeight: FontWeight.w300),),
              ),
              SizedBox(
            height: h * 0.03,
              ),
              SizedBox(
              height: h * 0.15,
              child: ButtonWidget(
                text: "Kullanıcı Girişi",
                color: Constants.greens,
                minusWidth: w * 0.3,
                isLoading: false,
                onPressed: () {
                  Navigator.push(
                      context,
                      // ignore: inference_failure_on_instance_creation
                      MaterialPageRoute(builder: (context) =>  LoginScreen()),
                    );
                },
                color2: Constants.greens2,
              ))
            ],
          ),
        ),
      ),
          ),
    );
  }
}
/*
SizedBox(
        height: h * 2,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: h * 0.1,
              ),
              SizedBox(
                  width: w * 0.6,
                  height: h * 0.4,
                  child: Image.asset(
                    "assets/atik_hesapla.png",
                    fit: BoxFit.contain,
                  )),
              
              SizedBox(
              child: Text("Hoşgeldiniz",style: TextStyle(fontSize: w * 0.113,fontWeight: FontWeight.w300),),
              ),
              SizedBox(
                height: h * 0.03,
              ),
              SizedBox(
                  height: h * 0.15,
                  child: ButtonWidget(
                    text: "Kullanıcı Girişi",
                    color: Constants.greens,
                    minusWidth: w * 0.3,
                    isLoading: false,
                    onPressed: () {
                      Navigator.push(
                          context,
                          // ignore: inference_failure_on_instance_creation
                          MaterialPageRoute(builder: (context) =>  LoginScreen()),
                        );
                    },
                    color2: Constants.greens2,
                  ))
            ],
          ),
        ),
      ),
*/