
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sifiratik/features/confirm/cubit/confirm_cubit.dart';
import 'package:sifiratik/features/home/cubit/home_cubit.dart';
import 'package:sifiratik/features/home/view/home_screen.dart';
import 'package:sifiratik/features/login/cubit/login_cubit.dart';
import 'package:sifiratik/features/recycle/cubit/calculation_cubit.dart';
import 'package:sifiratik/features/reports/cubit/report_cubit.dart';
import 'package:sifiratik/features/welcome/welcome_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.isUserLoggedIn}) : super(key: key);
  final String? isUserLoggedIn;
  // This widget is the root of your application.
  
  @override
  Widget build(BuildContext context) {
    return  MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => LoginCubit()),
        BlocProvider(create: (context) => HomeCubit()..getMunicipalityy()),
        BlocProvider(create: (context) => CalculationCubit()..getProducts()),
        BlocProvider(create: (context) => ConfirmCubit()),
        BlocProvider(create: (context) => ReportCubit()..onDateChange(date: DateTime.now()))
      
      ],
      child:  MaterialApp(
        debugShowCheckedModeBanner: false,
        home:  isUserLoggedIn !=null ? HomeScreen() :WelcomeScreen()
      ),
    );
  }
}