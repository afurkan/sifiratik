import 'package:sifiratik/constants/locator.dart';
import 'package:sifiratik/constants/secure_storage.dart';
import 'package:sifiratik/model/municipality_model.dart';
import 'package:sifiratik/service/home_service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(const HomeState());

  final HomeService _homeService = HomeService();
  final _secureStorage = getIt<SecureStorage>();
  Future<void> getMunicipalityy() async {
    emit(const HomeState(mainStateStatus: MainStateStatus.loading));
    try {
      final response = await _homeService.getMunicipality();
      final userName = await _secureStorage.findKey("userName");
      emit(HomeState(
        mainStateStatus: MainStateStatus.success,
        baseMunicipalityModel: response,
        userName: userName,
      ));
    } catch (e) {
      emit(
        const HomeState(mainStateStatus: MainStateStatus.error),
      );
    }
  }
}
