import 'dart:math';
import 'package:sifiratik/constants/locator.dart';
import 'package:sifiratik/constants/secure_storage.dart';
import 'package:sifiratik/model/selected_product_model.dart';
import 'package:dio/dio.dart';

class ConfirmationService {
  final Dio _dio = getIt<Dio>();
  final SecureStorage _secureStorage = getIt<SecureStorage>();
  Map isFull = {}, isFull2 = {};
  Future<dynamic> confirmUser({
    required String amount,
    required List<SelectedProductModel> list,
  }) async {
    const chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random rnd = Random();
    String getRandomString(int length) =>
        String.fromCharCodes(Iterable.generate(length, (_) => chars.codeUnitAt(rnd.nextInt(chars.length))));
    String orderKey = getRandomString(14);
    final userId = await _secureStorage.findKey('userId');
    final referenceCode = await _secureStorage.findKey('pinCode');

    for (var element in list) {
      double price = element.count * element.productPrice;
      Map<String, dynamic> element2 = {
        "product_name": element.productName,
        "product_field": element.count,
        "product_price": price,
        "product_id": element.productId,
      };
      isFull.addAll({element.productName: element2});
      isFull2.addAll({element.productId: element2});
    }

    try {
      final response = await _dio.post(
        '/orders/',
        data: {
          "user_id": userId,
          "order_key": orderKey,
          "products": isFull,
          "products_test": isFull2,
          "price": amount,
          "address": "Arapzade",
          "reference_code": referenceCode.toString()
        },
      );
      isFull.clear();
      isFull2.clear();
      return response.data;
     
    } catch (e) {}
  }
}
